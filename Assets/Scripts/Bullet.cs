﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{


    public GameObject hitEffect;


    void OnCollisionEnter2D(Collision2D collision)
    {

        var othertag = collision.gameObject.tag;
        if (othertag.Equals(gameObject.tag))
        {
            Destroy(gameObject);
        }

    }
   
}
