﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float moveSpeed = 5f;
    public Transform firePoint;

    public Rigidbody2D rb;
    public Camera cam;

    public GameObject StonePrefab;
    public GameObject PaperPrefab;
    public GameObject ScissorsPrefab;

    public float bulletForce = 20f;

    Vector2 movement;
    Vector2 mousePos;
//Vector2 MapCameraPos;

    void Update()
    {

        //if (isLocalPlayer == true)
        //{
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");

        


        if (Input.GetButtonDown("Fire1"))
            {
                ShootStone();
            }
            else if (Input.GetButtonDown("Fire2"))
            {
                ShootPaper();
            }
            else if (Input.GetButtonDown("Fire3"))
            {
                ShootScissors();
            }
        //}
    }

    void FixedUpdate()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
        //Debug.Log("Text: " + mousePos + angle);
    }

    
    public void ShootStone()
    {
        GameObject stone =  Instantiate(StonePrefab, firePoint.position, firePoint.rotation);
        //NetworkServer.SpawnWithClientAuthority(stone, connectionToClient);

        //NetworkManager.singleton.

        Rigidbody2D rb1 = stone.GetComponent<Rigidbody2D>();
        rb1.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
    }
    private void ShootPaper()
    {
        GameObject paper = Instantiate(PaperPrefab, firePoint.position, firePoint.rotation);
        //NetworkServer.SpawnWithClientAuthority(paper, connectionToClient);

        Rigidbody2D rb2 = paper.GetComponent<Rigidbody2D>();
        rb2.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
    }
    private void ShootScissors()
    {
        GameObject scissors = Instantiate(ScissorsPrefab, firePoint.position, firePoint.rotation);
        //NetworkServer.SpawnWithClientAuthority(scissors, connectionToClient);

        Rigidbody2D rb3 = scissors.GetComponent<Rigidbody2D>();
        rb3.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
    }


    //    movement.x = Input.GetAxisRaw("Horizontal");
    //    movement.y = Input.GetAxisRaw("Vertical");
    //    if (gameObject.tag == "obstacle")
    //    {
    //        Debug.Log("przeszkoda");
    //    }

    //    mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
    //}
    //void FixedUpdate()
    //{
    //    rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

    //    Vector2 lookDir = mousePos - rb.position;
    //    float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
    //    rb.rotation = angle;
}
